package pizzashop.food;

/** 
 * A drink has a size and flavor.
 */
public class Drink extends AbstractItem {
	// constants related to drink size
	public static final double [] prices = { 0.0, 20.0, 30.0, 40.0 };
	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	private String flavor;
	private int size;
	/**
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		this.size = size;
	}
	
	/** 
	 * Get a description of the Drink.
	 */
	public String toString() {
		return sizes[size] + " " + (flavor==null? "": flavor.toString()) + " drink"; 
	}

	/** return the price of a drink
	 * @see pizzashop.FoodItem#getPrice()
	 */
	public double getPrice() {
		return super.getPrice(size, prices);
	}
	
	public Object clone() {
		Drink clone;
		
		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		//superclass already cloned the size and flavor
		return clone;
	}

	/**
	 * Set a size of the drink.
	 * @param size is a new size
	 */
	public void setSize(int size) {
		this.size = size;
	}
}
