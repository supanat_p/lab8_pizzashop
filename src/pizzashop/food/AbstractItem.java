package pizzashop.food;
/**
 * Abstract class of items.
 * @author Supanat Pokturng
 * @version 2015.10.03
 */
public abstract class AbstractItem implements OrderItem{
	
	/**
	 * Get a price of the items.
	 * @param size is a size of the item
	 * @param prices is an array of a price
	 * @return a price of the item
	 */
	public double getPrice( int size , double [] prices ) {
		double price = 0.0;
		if (size >= 0 && size < prices.length)
			price = prices[size];
		return price;
	}

}
