package pizzashop.food;
/**
 * Interface if ordered item
 * @author Supanat Pokturng
 * @version 2015.10.03
 */
public interface OrderItem {
	double getPrice();
	void setSize( int size );
}
